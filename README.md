# grow

A fun distraction based on [This Patreon post by Dream&Nightmare](https://www.patreon.com/posts/growr-round-06-43765935).
It doesn't really *do* anything, but *shrug* fun anywho.
Sometime I may add earlier versions of the app as visible in the growr series.

![Screenshot](screenshot.jpg)
![Screencap](screencap.gif)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

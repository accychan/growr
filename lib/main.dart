import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GrowR',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class OptionData {
  int masculine = 0;
  int muscles = 0;
  int genitals = 0;
  int fat = 0;
  int libido = 0;
  int intelligence = 0;
  int pec = 0;
  int paw = 0;
  int body = 0;
  int ball = 0;
  int dick = 0;
  int cum = 0;

  @override
  String toString() {
    return 'Masculine: ${masculine.toString()}\n' +
        'Muscles: ${muscles.toString()}\n' +
        'Genitals: ${genitals.toString()}\n' +
        'Fat: ${fat.toString()}\n' +
        'Libido: ${libido.toString()}\n' +
        'Intelligence: ${intelligence.toString()}\n' +
        'Pec Size: ${pec.toString()}\n' +
        'Paw Size: ${paw.toString()}\n' +
        'Balls Size: ${ball.toString()}\n' +
        'Dick Size: ${dick.toString()}\n' +
        'Cum Production: ${cum.toString()}';
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    @required RenderBox parentBox,
    Offset offset = Offset.zero,
    @required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight;
    final double trackLeft = offset.dx;
    final double trackTop =
        offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}

class _MyHomePageState extends State<MyHomePage> {
  OptionData options = OptionData();
  bool _glitch = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Scaffold(
          backgroundColor: Color.fromRGBO(38, 55, 74, 1.0),
          appBar: AppBar(
            actions: [
              IconButton(
                icon: Icon(Icons.send),
                onPressed: () {
                  print(options.toString());
                  if (options.masculine > 160 &&
                      options.muscles > 160 &&
                      options.libido > 180 &&
                      options.intelligence > 125) {
                    print(
                        "There's a problem - An unexpected error occurred while executing the current operation. - Error Code: 80630049");
                    _showAlert(
                            title: "There's a problem",
                            text:
                                "An unexpected error occurred while executing the current operation.\nError Code: 80630049")
                        .then((_) {
                      setState(() {
                        _glitch = true;
                      });
                      _showAlert(
                              title: 'Abandon all hope',
                              text: 'Abandon all hope')
                          .then((_) {
                        setState(() {
                          options = OptionData();
                          _glitch = false;
                        });
                      });
                    });
                  }
                },
              )
            ],
            title: Center(
              child: Column(
                children: [
                  Text(
                    'GrowR',
                    textScaleFactor: 1.5,
                  ),
                  Text(
                    'developer build v0.44',
                    textScaleFactor: 0.5,
                  ),
                ],
              ),
            ),
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  child: Text('GrowR'),
                  decoration: BoxDecoration(
                    color: Colors.blueGrey,
                  ),
                ),
                RaisedButton(
                  child: Text('About'),
                  onPressed: () => _showAlert(
                      title: 'About', text: "I thew this app together as a fun exercise from Dream&Nightmare's GrowR series.", barrierDismissible: true),
                )
              ],
            ),
          ),
          body: SliderTheme(
            data: SliderThemeData(
              // See this to make them rounded rectangle: https://medium.com/flutter-community/flutter-sliders-demystified-4b3ea65879c
              thumbShape: RoundSliderThumbShape(
                enabledThumbRadius: 12.0,
              ),
              thumbColor: Colors.white,
              trackShape: CustomTrackShape(),
            ),
            child: DefaultTextStyle(
              style: TextStyle(color: Colors.white),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Masculine',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.masculine.toDouble(),
                              min: -200,
                              max: 200,
                              divisions: 5,
                              label: '${options.masculine}',
                              onChanged: (double val) {
                                setState(() {
                                  options.masculine = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Feminine\n${options.masculine}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'Bigger\nMuscles',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.muscles.toDouble(),
                              min: -200,
                              max: 200,
                              label: '${options.muscles}',
                              divisions: 5,
                              onChanged: (double val) {
                                setState(() {
                                  options.muscles = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Smaller\nMuscles\n${options.muscles}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'Larger\nGenitals',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.genitals.toDouble(),
                              min: -200,
                              max: 200,
                              label: '${options.genitals}',
                              divisions: 5,
                              onChanged: (double val) {
                                setState(() {
                                  options.genitals = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Smaller\nGenitals\n${options.genitals}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'More\nBody Fat',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.fat.toDouble(),
                              min: -200,
                              max: 200,
                              label: '${options.fat}',
                              divisions: 5,
                              onChanged: (double val) {
                                setState(() {
                                  options.fat = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Less\nBody Fat\n${options.fat}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'High\nLibido',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.libido.toDouble(),
                              min: -200,
                              max: 200,
                              label: '${options.libido}',
                              divisions: 5,
                              onChanged: (double val) {
                                setState(() {
                                  options.libido = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Low\nLibido\n${options.libido}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            'High\nIntelligence',
                            textAlign: TextAlign.center,
                          ),
                          RotatedBox(
                            quarterTurns: 3,
                            child: Slider(
                              value: options.intelligence.toDouble(),
                              min: -200,
                              max: 200,
                              label: '${options.intelligence}',
                              divisions: 5,
                              onChanged: (double val) {
                                setState(() {
                                  options.intelligence = val.round();
                                });
                              },
                            ),
                          ),
                          Text(
                            'Low\nIntelligence\n${options.intelligence}',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Center(
                    child: Text(
                      'Advanced Options',
                      textScaleFactor: 1.5,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Pec Size',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.pec.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.pec}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.pec = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                              Text(
                                'Paw Size',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.paw.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.paw}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.paw = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                              Text(
                                'Body Size',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.body.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.body}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.body = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Ball Size',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.ball.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.ball}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.ball = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                              Text(
                                'Dick Size',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.dick.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.dick}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.dick = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                              Text(
                                'Cum/Precum Production',
                                textScaleFactor: 1.2,
                              ),
                              Slider(
                                value: options.cum.toDouble(),
                                min: -200,
                                max: 200,
                                label: '${options.cum}',
                                divisions: 5,
                                onChanged: (double val) {
                                  setState(() {
                                    options.cum = val.round();
                                  });
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Small',
                                    textScaleFactor: 0.8,
                                  ),
                                  Text(
                                    'Large',
                                    textScaleFactor: 0.8,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        if (_glitch)
          Image.asset(
            'images/Glitch_Art.jpg',
            fit: BoxFit.cover,
          ),
      ],
    );
  }

  Future<void> _showAlert(
      {String title, String text, bool barrierDismissible = false}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return SimpleDialog(
          contentPadding: EdgeInsets.all(8.0),
          title: Text(title),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(text),
            ),
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
